import dotenv  from "dotenv";
import { env } from "process";

import express from "express";
import router from "./Router";

dotenv.config();
const app = express();

app.use(express.json());
app.use(express.urlencoded());
app.use("/", router);

app.listen(env.SERVER_PORT, () =>{ 
    console.log(`STARTING...`);
    console.log(`Listening on port ${env.SERVER_PORT}`);
});